$truffle migrate --reset //para resetear mis contratos (en realidad las migraciones estan pensadas para hacer una vez, pero con esto podemos ahora para seguir probando)
>Contract.deployed().then(function(i) { app=i;}) //para desplegar una instancia de nuestro contrato y asignar una variable
>app.students(1) //funcion gratis que tenemos al declarar public estudiantes, podemos ver el primer estudiante de la lista pasandole la key 1
>app.studentsCount()
>app.students(1).then(function(c) {student=c;})
>student //devuelve el estudiante que defini antes
>student[1] //con indice base cero obtengo en este caso el nombre del estudiante 
>student[2].toNumber() //devuelve la nota en numero. Todo esto para hacer fetch al estudiante desde consola
>web3.eth.getAccounts() //para obtener las cuentas de Ganache que estan conectadas a nuestra network

Mocha framework for the test.
Chai 	
seed words: unique grocery assist garage wage method photo shadow vacant thought nuclear lucky