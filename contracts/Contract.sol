pragma solidity ^0.5.0;

contract Contract {
    //Model a student 
    struct Student {
        uint id;
        string name;
        uint grade;
    }
    //Store Student
    //Fetch Student
    mapping(uint => Student) public students;//para acceder a cada estudiante
    //Store student grade
    uint public studentsCount;

    constructor () public {
        addStudent("estudiante 1"); 
        addStudent("estudiante 2");
    }
/**
    contract Memory {
        function addTransaction(address destination, uint value, bytes memory)
        internal
        returns (uint transactionId)
        {
            return 0;
        }
    }
 */
    function addStudent (string memory _name) private {
        studentsCount ++;
        students[studentsCount] = Student(studentsCount, _name, 0); //le paso el id que es el count y nombre e inicialmente como nota 0
    }
}